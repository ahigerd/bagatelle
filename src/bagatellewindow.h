#ifndef BAGATELLEWINDOW_H
#define BAGATELLEWINDOW_H

#include <QMainWindow>

class BagatelleBoard;
class QLabel;

class BagatelleWindow : public QMainWindow
{
Q_OBJECT

public:
    BagatelleWindow();

public slots:
    void addScore(int points);
    void newGame();
    void about();

private slots:
    void pegActivated(const QPoint& point);
    void pegStruck(const QPoint& point);
    void ballUsed();
    void ballLost();
    void boardCleared();

private:
    QLabel* score;
    QLabel* balls;
    BagatelleBoard* board;
    QAction* soundEnabled;
    int comboLength;
};

#endif // BAGATELLEWINDOW_H
