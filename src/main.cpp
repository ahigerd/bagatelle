#include <QtGui/QApplication>
#include "bagatellewindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("Bagatelle");
    a.setApplicationVersion("1.0.0");
    a.setOrganizationDomain("org.alkahest");
    a.setOrganizationName("Alkahest File Enterprises");

    BagatelleWindow w;
    w.show();
    return a.exec();
}
