#ifndef MACSOUND_H
#define MACSOUND_H

#include <QFile>
#include <QString>

class MacSound
{
public:
    MacSound();
    ~MacSound();

    void open(const QString& filename);
    void play();

private:
    QFile data;
    uchar* map;
};

#endif // MACSOUND_H
