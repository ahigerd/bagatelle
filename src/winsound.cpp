#include "winsound.h"
#include <mmsystem.h>

WinSound::WinSound() : map(0)
{
}

WinSound::~WinSound()
{
}

void WinSound::open(const QString& filename)
{
    data.setFileName(filename);
    data.open(QIODevice::ReadOnly);
    map = data.map(0, data.size());
}

void WinSound::play()
{
    if(!map) return;
    sndPlaySoundA(reinterpret_cast<CHAR*>(map), SND_MEMORY | SND_ASYNC);
}
