TARGET = Bagatelle
TEMPLATE = app
DESTDIR = ..
INCLUDEPATH += ../box2d/Include
LIBS += -L.. \
    -lbox2d
SOURCES += main.cpp \
    bagatelleboard.cpp \
    bagatellewindow.cpp
HEADERS += bagatelleboard.h \
    bagatellewindow.h
VERSION = 1.0.0
macx { 
    SOURCES += macsound.mm
    HEADERS += macsound.h
    LIBS += -framework \
        AppKit \
        -framework \
        Foundation
    ICON = Bagatelle.icns
    OTHER_FILES += Bagatelle.Info.plist # for Qt Creator's sake
}
win32 { 
    SOURCES += winsound.cpp
    HEADERS += winsound.h
    LIBS += -lwinmm
    RC_FILE = bagatelle.rc
    OTHER_FILES += bagatelle.rc
}
RESOURCES += bagatelle.qrc
