#include "bagatelleboard.h"
#include <QPaintEvent>
#include <QMouseEvent>
#include <QTimer>
#include <QPainter>
#include <QList>
#include <stddef.h>
#include <QLinearGradient>
#include <QApplication>
#include <QSettings>

#ifdef Q_OS_MAC
#include "macsound.h"
MacSound soundOne, soundTen;
#endif
#ifdef Q_OS_WIN
#include "winsound.h"
WinSound soundOne, soundTen;
#endif

QImage makeBallGradient()
{
    QImage ballGradient(16, 16, QImage::Format_ARGB32_Premultiplied);
    QPainter p(&ballGradient);
    ballGradient.fill(0);
    QColor c(255, 255, 255);
    for(int i = 0; i < 16; i++) {
        c.setAlpha(4 * (i+1));
        p.setBrush(c);
        p.setPen(c);
        p.drawEllipse(QPointF(8.0 - (i / 3.0), 8.0 - (i / 3.0)), 8 - i / 2.0, 8 - i / 2.0);
    }
    return ballGradient;
}

static void setBallColor(b2Fixture* b, const QColor& c)
{
    QColor* curr = reinterpret_cast<QColor*>(b->GetUserData());
    if(*curr == c) return; // no need to change to the color it already is
    delete curr;
    b->SetUserData(new QColor(c));
}

static QPixmap generateBackground()
{
    QImage bgImg(400, 400, QImage::Format_RGB32);
    QLinearGradient g(0, 0, 400, 0);
    g.setColorAt(0.0, QColor(0, 0, 72));
    g.setColorAt(0.15, QColor(0, 0, 36));
    g.setColorAt(0.5, Qt::black);
    g.setColorAt(0.85, QColor(0, 0, 36));
    g.setColorAt(1.0, QColor(0, 0, 72));
    QPainter painter(&bgImg);
    painter.fillRect(0, 0, 400, 400, g);
    return QPixmap::fromImage(bgImg);
}

BagatelleBoard::BagatelleBoard(QWidget *parent)
: QWidget(parent), world(0), ballBody(0), destroyFrame(0), useSound(true)
{
    soundOne.open(":/sounds/onepoint.wav");
    soundTen.open(":/sounds/tenpoints.wav");
    qsrand(::time(0));

    b2AABB worldAABB;
    worldAABB.lowerBound.Set(-100, -100);
    worldAABB.upperBound.Set(100, 100);
    b2Vec2 gravity(0, -20);
    bool doSleep = true;
    world = new b2World(worldAABB, gravity, doSleep);
    world->SetContactListener(this);

    initializePegs();
    background = generateBackground();
    ballGradient = makeBallGradient();

    setFixedSize(400, 400);

    timer = new QTimer(this);
    timer->setInterval(1000.0 / 120.0);
    QObject::connect(timer, SIGNAL(timeout()), this, SLOT(timeStep()));
    timer->start();
}

BagatelleBoard::~BagatelleBoard()
{
    delete world;
}

void BagatelleBoard::initializePegs()
{
    if(pegsRemaining != 0) {
        for(b2Body* b = world->GetBodyList(); b; b = b->GetNext()) {
            b2Fixture* nextFixture = 0;
            for(b2Fixture* f = b->GetFixtureList(); f; f = nextFixture) {
                nextFixture = f->GetNext();
                b->DestroyFixture(f);
            }
        }
    }

    if(ballBody) world->DestroyBody(ballBody);
    ballBody = 0;

    b2Body* groundBody = world->GetGroundBody();
    b2Fixture* buffer;
    double x, y;
    b2AABB query;
    for(int i = 0; i < 30; i++) {
        do {
            x = (qrand() / double(RAND_MAX)) * 90.0 - 45.0;
            y = (qrand() / double(RAND_MAX)) * 90.0 - 45.0;
            query.upperBound.Set(x+2, y+2);
            query.lowerBound.Set(x-2, y-2);
        } while(world->Query(query, &buffer, 1));

        b2CircleDef circleDef;
        circleDef.localPosition.Set(x, y);
        circleDef.radius = 2.0;
        b2Fixture* f = groundBody->CreateFixture(&circleDef);
        f->SetUserData(new QColor(255, 0, 0));
    }
    pegsRemaining = 30;
    update();
}

void BagatelleBoard::launchBall(double x, double y, double dx, double dy)
{
    if(ballBody) world->DestroyBody(ballBody);
    stuckFrames = 0;

    b2BodyDef ballDef;
    ballBody = world->CreateBody(&ballDef);
    b2CircleDef ballCircleDef;
    ballCircleDef.radius = 2.0;
    ballCircleDef.density = 1.0;
    ballCircleDef.friction = 0.0;
    ballCircleDef.restitution = 1.0005;
    ballFixture = ballBody->CreateFixture(&ballCircleDef);
    ballFixture->SetUserData(new QColor(128, 128, 160));
    ballBody->SetMassFromShapes();

    ballBody->SetPosition(b2Vec2(x, y));
    ballBody->SetLinearVelocity(b2Vec2(dx, dy));

    emit ballUsed();
}

void BagatelleBoard::timeStep()
{
    // calculate the new state of the world
    world->Step(1.0/90.0, 10, 10);

    // animate pin destruction?
    if(destroyFrame != 0) {
        --destroyFrame;
        if(destroyFrame == 0) {
            // fade animation is complete
            destroyPegs();
            if(stuckFrames < 30) {
                if(ballBody) {
                    world->DestroyBody(ballBody);
                    ballBody = 0;
                }
                if(pegsRemaining == 0)
                    emit boardCleared();
                else
                    emit ballLost();
            }
        }

        update();
        return;
    }

    if(ballBody) {
        // Check for wall bounces
        // TODO: use objects
        b2Vec2 pos = ballBody->GetPosition();
        b2Vec2 vel = ballBody->GetLinearVelocity();
        if(pos.x < -48.0 || pos.x > 48.0) {
            vel.x = -vel.x;
            ballBody->SetLinearVelocity(vel);
        }
        if(pos.y > 90.0) {
            vel.y = -vel.y;
            ballBody->SetLinearVelocity(vel);
        }

        // Check to see if the ball has fallen off the bottom
        if(pos.y < -50.0) {
            destroyFrame = 64;
        }

        // check for stuck
        if(destroyFrame == 0) {
            if(stuckFrames == 0) {
                stuckPos = pos;
                stuckFrames++;
            } else {
                if((stuckPos - pos).Length() < 0.25) {
                    stuckFrames++;
                } else {
                    stuckFrames = 0;
                }
                if(stuckFrames > 30)
                    destroyFrame = 64;
            }
        }
    }

    // Redraw the screen
    update();
}

void BagatelleBoard::destroyPegs()
{
    for(b2Body* b = world->GetBodyList(); b; b = b->GetNext()) {
        if(b == ballBody) continue;
        QList<b2Fixture*> destroyList;
        for(b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
            QColor* c = reinterpret_cast<QColor*>(f->GetUserData());
            if(*c != Qt::red) {
                destroyList << f;
                delete c;
            }
        }
        foreach(b2Fixture* f, destroyList) {
            b->DestroyFixture(f);
            --pegsRemaining;
        }
    }
}

void BagatelleBoard::paintEvent(QPaintEvent* event)
{
    Q_UNUSED(event);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    painter.drawPixmap(0, 0, background);

    if(!dragStart.isNull()) {
        painter.setPen(QPen(Qt::white, 0, Qt::DashLine));
        painter.drawLine(dragStart, dragCurrent);
        painter.setPen(QColor(255, 255, 255, 128));
        painter.setBrush(QColor(128, 128, 160, 128));
        painter.drawEllipse(dragCurrent, 8, 8);
    }

    painter.setPen(Qt::black);
    painter.translate(192.0, 200.0);

    QImage fadeImage = ballGradient;
    if(destroyFrame != 0) {
        QPainter fade(&fadeImage);
        fade.setCompositionMode(QPainter::CompositionMode_DestinationIn);
        fade.fillRect(fadeImage.rect(), QColor(0, 0, 0, destroyFrame * 4 - 1));
    }

    for(b2Body* b = world->GetBodyList(); b; b = b->GetNext()) {
        float bx = b->GetPosition().x;
        float by = b->GetPosition().y;
        for(b2Fixture* f = b->GetFixtureList(); f; f = f->GetNext()) {
            b2CircleShape* c = static_cast<b2CircleShape*>(f->GetShape());
            QColor color = *reinterpret_cast<QColor*>(f->GetUserData());
            if(destroyFrame != 0) {
                if(color != Qt::red && f != ballFixture) {
                    painter.setPen(QColor(0, 0, 0, destroyFrame*4-1));
                    color.setAlpha(destroyFrame*4-1);
                } else {
                    painter.setPen(Qt::black);
                }
            } else if(color.red() != 255 && color.red() != 0 && f != ballFixture) {
                setBallColor(f, QColor(color.red() - 3, color.green() - 1, color.blue() - 3));
            }
            painter.setBrush(color);
            float x = bx + c->m_p.x;
            float y = by + c->m_p.y;
            painter.drawEllipse(x * 4, y * -4, int(c->m_radius*8), int(c->m_radius*8));
            if(color.alpha() == 255) {
                painter.drawImage(x * 4, y * -4, ballGradient);
            } else {
                painter.drawImage(x * 4, y * -4, fadeImage);
            }
        }
    }
    int ct = scoreAnims.count();
    if(!ct) return;
    QList<int> toRemove;
    for(int i = 0; i < ct; i++) {
        ScoreAnim& a = scoreAnims[i];
        double vel = (0.2 + a.points / 100.0);
        if(a.staticMessage)
            vel = 0;
        else if(vel > 1.5)
            vel = 1.5;
        double size = 8 + (a.points / 10);
        if(!a.staticMessage && size > 24)
            size = 24;
        int fadeRate = (a.staticMessage ? 1 : 2);
        QRectF rect(a.pos.x() - a.radius, a.pos.y() - 16 - (a.frame * vel), a.radius * 2 + 16, 48);
        QTextOption opt(Qt::AlignCenter);
        QFont f = painter.font();
        f.setPixelSize(size);
        painter.setFont(f);
        painter.setPen(QColor(255, 255, 0, 255-(a.frame * fadeRate)));
        painter.drawText(rect, a.text, opt);
        a.frame++;
        if(a.frame > (255 / fadeRate)) toRemove.prepend(i);
    }
    foreach(int i, toRemove) scoreAnims.removeAt(i);
}

void BagatelleBoard::mousePressEvent(QMouseEvent* event)
{
    if(!isEnabled() || ballBody || !pegsRemaining || destroyFrame || !scoreAnims.isEmpty()) return;
    dragStart = dragCurrent = event->pos();
}

void BagatelleBoard::mouseMoveEvent(QMouseEvent* event)
{
    if(dragStart.isNull()) return;
    dragCurrent = event->pos();
}

void BagatelleBoard::mouseReleaseEvent(QMouseEvent* event)
{
    if(dragStart.isNull()) return;
    int dx = dragStart.x() - event->x();
    int dy = dragStart.y() - event->y();
    int x = event->x();
    if(x < 8) x = 8;
    if(x > 392) x = 392;
    launchBall((x / 4.0) - 50.0, 53.0 - (event->y() / 4.0), dx / 1.5, dy / -1.5);
    dragStart = QPoint();
}

void BagatelleBoard::triggerPeg(b2Fixture* peg)
{
    if(peg == ballFixture) return;
    b2Vec2 bodyPos = peg->GetBody()->GetPosition();
    b2Vec2 shapePos = static_cast<b2CircleShape*>(peg->GetShape())->m_p;
    QPoint point((bodyPos.x + shapePos.x) * 4.0, (bodyPos.y + shapePos.y) * -4.0);
    if(reinterpret_cast<QColor*>(peg->GetUserData())->red() == 255) {
        emit pegActivated(point);
        if(useSound) soundTen.play();
    } else {
        emit pegStruck(point);
        if(useSound) soundOne.play();
    }
    setBallColor(peg, QColor(225,255,225));
}

void BagatelleBoard::BeginContact(b2Contact* point)
{
    triggerPeg(point->GetFixtureA());
    triggerPeg(point->GetFixtureB());
}

void BagatelleBoard::showScore(int points, const QPoint& center)
{
    scoreAnims << ScoreAnim(points, center);
}

void BagatelleBoard::showMessage(const QString& message)
{
    ScoreAnim anim(300, QPoint(0, -120));
    anim.text = message;
    anim.staticMessage = true;
    anim.radius = 150;
    scoreAnims << anim;
}

void BagatelleBoard::setSoundEnabled(bool on)
{
    QSettings settings;
    settings.setValue("soundEnabled", QVariant::fromValue(on));
    useSound = on;
}

ScoreAnim::ScoreAnim(int points, const QPoint& pos) : points(points), frame(0), pos(pos), staticMessage(false), radius(32) {
    text = QString::number(points);
}
