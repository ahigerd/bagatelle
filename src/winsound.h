#ifndef WINSOUND_H
#define WINSOUND_H

#include <QFile>
#include <QString>

class WinSound
{
public:
    WinSound();
    ~WinSound();

    void open(const QString& filename);
    void play();

private:
    QFile data;
    uchar* map;
};

#endif // WINSOUND_H
