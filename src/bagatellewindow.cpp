#include "bagatellewindow.h"
#include "bagatelleboard.h"
#include <QBoxLayout>
#include <QLabel>
#include <QTimer>
#include <QMenuBar>
#include <QMenu>
#include <QKeySequence>
#include <QApplication>
#include <QSettings>
#include <QMessageBox>
#include <QIcon>

BagatelleWindow::BagatelleWindow()
{
    QSettings settings;

    QIcon icon;
    icon.addFile(":/images/128x128.png", QSize(128,128));
    icon.addFile(":/images/64x64.png", QSize(64,64));
    icon.addFile(":/images/32x32.png", QSize(32,32));
    icon.addFile(":/images/16x16.png", QSize(16,16));
#ifndef Q_OS_MAC
    setWindowIcon(icon);
#endif

    setWindowTitle("Bagatelle");
    QWidget* center = new QWidget(this);
    QVBoxLayout* layout = new QVBoxLayout(center);

    QHBoxLayout* hl = new QHBoxLayout;
    hl->addWidget(new QLabel("Score:", center), 0);
    score = new QLabel("0", center);
    hl->addWidget(score, 1);
    hl->addWidget(new QLabel("Balls:", center), 0);
    balls = new QLabel("10", center);
    hl->addWidget(balls, 0);
    layout->addLayout(hl, 0);

    board = new BagatelleBoard(this);
    layout->addWidget(board, 0, Qt::AlignCenter);
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    setCentralWidget(center);

    QMenuBar* mb = new QMenuBar(this);
    setMenuBar(mb);
    QMenu* game = new QMenu("&Game", mb);
    game->addAction("&New Game", this, SLOT(newGame()), QKeySequence("Ctrl+N"));
    soundEnabled = game->addAction("Enable &Sound");
    soundEnabled->setCheckable(true);
    soundEnabled->setChecked(settings.value("soundEnabled", QVariant::fromValue(true)).toBool());
    game->addSeparator();
    game->addAction("&About Bagatelle...", this, SLOT(about()));
    game->addAction("About Qt...", qApp, SLOT(aboutQt()));
    game->addSeparator();
    game->addAction("&Quit", qApp, SLOT(quit()), QKeySequence("Ctrl+Q"));
    mb->addMenu(game);

    QObject::connect(board, SIGNAL(pegActivated(QPoint)), this, SLOT(pegActivated(QPoint)));
    QObject::connect(board, SIGNAL(pegStruck(QPoint)), this, SLOT(pegStruck(QPoint)));
    QObject::connect(board, SIGNAL(ballUsed()), this, SLOT(ballUsed()));
    QObject::connect(board, SIGNAL(ballLost()), this, SLOT(ballLost()));
    QObject::connect(board, SIGNAL(boardCleared()), this, SLOT(boardCleared()));
    QObject::connect(soundEnabled, SIGNAL(toggled(bool)), board, SLOT(setSoundEnabled(bool)));
}

void BagatelleWindow::pegActivated(const QPoint& point)
{
    comboLength++;
    addScore(10 * comboLength);
    board->showScore(10 * comboLength, point);
}

void BagatelleWindow::pegStruck(const QPoint& point)
{
    addScore(1);
    board->showScore(1, point);
}

void BagatelleWindow::ballUsed()
{
    balls->setText(QString::number(balls->text().toInt() - 1));
    comboLength = 0;
}

void BagatelleWindow::boardCleared()
{
    addScore(100 * balls->text().toInt());
    balls->setText("10");
    board->showMessage("Board Cleared");
    QTimer::singleShot(1500, board, SLOT(initializePegs()));
}

void BagatelleWindow::ballLost()
{
    if(balls->text() == "0") {
        board->setEnabled(false);
        board->showMessage("Game Over");
    } else if(comboLength == 0) {
        board->showMessage("No Pegs Hit");
    }
}

void BagatelleWindow::addScore(int points)
{
    score->setText(QString::number(score->text().toInt() + points));
}

void BagatelleWindow::newGame()
{
    balls->setText("10");
    score->setText("0");
    board->initializePegs();
    board->setEnabled(true);
}

void BagatelleWindow::about()
{
    QMessageBox::about(this, "Bagatelle", "Bagatelle v"+QApplication::applicationVersion()+"\r\n"
                       "Copyright 2009 Alkahest File Enterprises\r\n\r\n"
                       "This program is free software; you can redistribute it and/or "
                       "modify it under the terms of the GNU General Public License as "
                       "published by the Free Software Foundation; either version 2 of "
                       "the License, or (at your option) any later version.\r\n\r\n"
                       "This program is distributed WITHOUT ANY WARRANTY; without even "
                       "the implied warranty of MERCHANTABILITY or FITNESS FOR A "
                       "PARTICULAR PURPOSE. See the GNU General Public License for more "
                       "details.\r\n\r\n"
                       "You should have received a copy of the GNU General Public License "
                       "along with this program; if not, write to:\r\n"
                       "Free Software Foundation, Inc.,\r\n51 Franklin Street, Fifth Floor,\r\n"
                       "Boston, MA  02110-1301  USA\r\n\r\n"
                       "Sounds derived from files from\r\n<http://free-loops.com>.");
}
