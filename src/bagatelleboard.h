#ifndef BAGATELLEBOARD_H
#define BAGATELLEBOARD_H

#include <QWidget>
#include <QPixmap>
#include <QImage>
#include <Box2D.h>
#include <QSound>

class QTimer;

struct ScoreAnim
{
    ScoreAnim(int points, const QPoint& pos);
    int points;
    QString text;
    int frame;
    QPoint pos;
    bool staticMessage;
    int radius;
};

class BagatelleBoard : public QWidget, public b2ContactListener
{
Q_OBJECT
public:
    BagatelleBoard(QWidget *parent = 0);
    ~BagatelleBoard();

    void BeginContact(b2Contact* point);

public slots:
    void timeStep();
    void initializePegs();
    void showScore(int points, const QPoint& center);
    void showMessage(const QString& message);
    void setSoundEnabled(bool on);

signals:
    void pegActivated(const QPoint&);
    void pegStruck(const QPoint&);
    void ballUsed();
    void ballLost();
    void boardCleared();

protected:
    void paintEvent(QPaintEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseMoveEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

private:
    void launchBall(double x, double y, double dx, double dy);
    void destroyPegs();
    void triggerPeg(b2Fixture* peg);

    QTimer* timer;
    b2World* world;
    b2Body* ballBody;
    b2Fixture* ballFixture;
    int destroyFrame, pegsRemaining;
    QPixmap background;
    QImage ballGradient;

    QPoint dragStart, dragCurrent;
    QList<ScoreAnim> scoreAnims;

    int stuckFrames;
    b2Vec2 stuckPos;
    bool useSound;
};

#endif // BAGATELLEBOARD_H
