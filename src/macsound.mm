#include "macsound.h"
#include <AppKit/AppKit.h>
#include <Foundation/NSData.h>

MacSound::MacSound() : map(0)
{
}

MacSound::~MacSound()
{    
}

void MacSound::open(const QString& filename)
{
    data.setFileName(filename);
    data.open(QIODevice::ReadOnly);
    map = data.map(0, data.size());
}

void MacSound::play()
{
    if(!map) return;
    NSData* nsdata = [[NSData alloc] initWithBytesNoCopy:map length:data.size() freeWhenDone:false];
    NSSound* sound = [[NSSound alloc] initWithData: nsdata];
    [sound play];
}
